orient = 0
rest="rest"
gotowork="gotowork"
work="work"
function init()
    state = rest
    robot.turret.set_position_control_mode()
end

function step()
    if state == "rest" then
        orient = listen()
    elseif state == "gotowork" then
        log("good")
    end
end

function reset()

end

function destroy()

end

--listen during rest state
function listen()
    if robot.range_and_bearing[1].data[1] == 1 then
        robot.wheels.set_velocity(10,-10)
        --state = gotowork
    end
end
