count_time = 0
msg0 = {0,0,0,0,0,0,0,0,0,0}
msg1 = {1,2,3,4,5,6,7,8,9,10}
roaming="roaming"
found="found"
grabbing="grabbing"
waiting="waiting"
working="working"
function init()
    robot.colored_blob_omnidirectional_camera.enable()
    robot.turret.set_position_control_mode()
    state=roaming
end

function step()
    log(state)
    if state == "roaming" then
        roam()
    elseif state == "found" then
        find()
    elseif state == "grabbing" then
        grab()
    elseif state == "waiting" then
        wait()
    elseif state == "working" then
        work()
    end
end

function reset()
    robot.colored_blob_omnidirectional_camera.enable()
    state = roaming
end

function destroy()
    robot.colored_blob_omnidirectional_camera.disable()
end



-- state of roaming
--here we avoid obstacles and roam till we find a light source
--and change to found state
function roam()
    sensingLeft=robot.proximity[3].value+robot.proximity[4].value+robot.proximity[5].value+robot.proximity[6].value+robot.proximity[2].value+robot.proximity[1].value
    sensingRight=robot.proximity[19].value+robot.proximity[20].value+robot.proximity[21].value+robot.proximity[22].value+robot.proximity[24].value+robot.proximity[23].value
    if #robot.colored_blob_omnidirectional_camera >= 1 then
        state = found
    elseif(sensingLeft ~=0)then
        robot.wheels.set_velocity(7,3)
    elseif(sensingRight ~= 0)then
        robot.wheels.set_velocity(3,7)
    else
        robot.wheels.set_velocity(10,10)
    end
end

--state found
--in this state we find the obstacle
--we go near it grab the obstacle and
--change state to wait
function find()
    orient=angle()
    if math.abs(ang) >= 0.1 then
        set_orient(ang)
    elseif math.abs(ang) < 0.1 then
        state = grabbing
        robot.wheels.set_velocity(2,2)
    end
end


--angle function
--we want to find the orientation the minimum distance light source makes to
--the robot
function angle()
    dist = robot.colored_blob_omnidirectional_camera[1].distance
    ang =  robot.colored_blob_omnidirectional_camera[1].angle
    for i = 1, #robot.colored_blob_omnidirectional_camera do
        if dist > robot.colored_blob_omnidirectional_camera[i].distance then
            dist = robot.colored_blob_omnidirectional_camera[i].distance
            ang = robot.colored_blob_omnidirectional_camera[i].angle
        end
    end
    return ang
end


--set_orient
--in this function we try to set the robot to correct orientation
function set_orient(ang)
    if ang>=0 then
        robot.wheels.set_velocity(-1,1)
    end
    if ang<=0 then
        robot.wheels.set_velocity(1,-1)
    end
end



--grab
--we just go grab it
function grab()
    grip_ang=200
    for i =1,24 do
        if robot.proximity[i].value == 1 then
            grip_ang = robot.proximity[i].angle
            break
        end
    end
    if grip_ang == 200 then
        robot.wheels.set_velocity(2,2)
    else
        robot.wheels.set_velocity(0,0)
        robot.turret.set_rotation(grip_ang)
        count_time = count_time + 1
    end
    if count_time == 100 then
        robot.gripper.lock_positive()
        robot.turret.set_passive_mode()
        count_time = 0
        state = waiting
    end
end


--wait state
--Call the other robot
--and wait
function wait()
    robot.leds.set_all_colors("green")
    robot.range_and_bearing.set_data(msg)
end
